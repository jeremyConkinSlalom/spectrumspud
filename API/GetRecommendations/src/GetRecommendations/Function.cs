using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;

using Amazon.DynamoDBv2;
using Amazon;
using Amazon.DynamoDBv2.DocumentModel;
using Newtonsoft.Json.Linq;
using Amazon.Lambda.APIGatewayEvents;


// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace GetRecommendations
{
    public class Function
    {
        private const string REGION = "us-east-2";
        private const string TABLE = "recommendations";
        
        [LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
        public async Task<APIGatewayProxyResponse> FunctionHandler(APIGatewayProxyRequest input, ILambdaContext context)
        {
            if(input.PathParameters == null ||
                !input.PathParameters.ContainsKey("id") || 
                string.IsNullOrWhiteSpace(input.PathParameters["id"]))
                return new APIGatewayProxyResponse
                {
                    Body = null,
                    Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
                    StatusCode = 400
                };

            string json = null;
            using(var client = new AmazonDynamoDBClient(RegionEndpoint.GetBySystemName(REGION)))
            {
                var table = Table.LoadTable(client, TABLE);
                var cfg = new GetItemOperationConfig();
                var item = await table.GetItemAsync(input.PathParameters["id"]);
                json = item?.ToJson();
            }

            if(json != null)
            {
                return new APIGatewayProxyResponse
                {
                    Body = json,
                    Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
                    StatusCode = 200,
                };
            }
            
            return new APIGatewayProxyResponse
            {
                Body = null,
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
                StatusCode = 404
            };
        }
    }
}
