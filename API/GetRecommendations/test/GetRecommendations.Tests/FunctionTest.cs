using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using GetRecommendations;
using Amazon.Lambda.APIGatewayEvents;

namespace GetRecommendations.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestValidId()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            var value = function.FunctionHandler(
                new APIGatewayProxyRequest{
                    PathParameters = new Dictionary<string,string>{{"id", "1234"}}}, context).Result;

            Assert.NotNull(value);
            Assert.NotNull(value.Body);
            Assert.Equal(200, value.StatusCode);
        }

        [Fact]
        public void TestInvalidId()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            var value = function.FunctionHandler(
                new APIGatewayProxyRequest{
                    PathParameters = new Dictionary<string,string>{{"id", "9999"}}}, context).Result;

            Assert.NotNull(value);
            Assert.Null(value.Body);
            Assert.Equal(404, value.StatusCode);
        }

         [Fact]
        public void TestNoParam()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            var value = function.FunctionHandler(
                new APIGatewayProxyRequest{
                    PathParameters = new Dictionary<string,string>()}, context).Result;

            Assert.NotNull(value);
            Assert.Null(value.Body);
            Assert.Equal(400, value.StatusCode);
        }
    }
}
