using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace PostRecommendation
{
    public class Function
    {
        public struct NewRecommendation
        {
            public string subject_id;
            public string subject_name;
            public string date;
            public string recommendation;
            public string recommender_id;
            public string recommender_name;
        }

        
        private const string REGION = "us-east-2";
        private const string TABLE = "recommendations";
        
        [LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
        public async Task<APIGatewayProxyResponse> FunctionHandler(APIGatewayProxyRequest apiRequest, ILambdaContext context)
        {
            NewRecommendation recommendation;
            try
            {
                //Should do better validation eventually.
                recommendation = JsonConvert.DeserializeObject<NewRecommendation>(apiRequest.Body);
            }
            catch
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = 400
                };
            }

            using(var client = new AmazonDynamoDBClient(RegionEndpoint.GetBySystemName(REGION)))
            {
                var newRecommendation = new AttributeValue 
                {
                    L = new List<AttributeValue>{new AttributeValue(){
                    M = {
                            {"date", new AttributeValue{S = recommendation.date}},
                            {"recommendation", new AttributeValue{S = recommendation.recommendation}},
                            {"recommender_id", new AttributeValue{S = recommendation.recommender_id}},
                            {"recommender_name", new AttributeValue{S = recommendation.recommender_name}}
                        }
                    }
                    }
                };

                var request = new UpdateItemRequest
                {
                    TableName = TABLE,
                    Key = new Dictionary<string,AttributeValue>() { { "subject_id", new AttributeValue { S =  recommendation.subject_id} } },
                    ExpressionAttributeNames = new Dictionary<string,string>()
                    {
                        {"#R", "recommendations"},
                        {"#I", "recommender_id"},
                        {"#N", "recommender_name"}
                    },
                    ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                    {
                        {":recc", newRecommendation},
                        {":id", new AttributeValue{S = recommendation.subject_id}},
                        {":name", new AttributeValue{S = recommendation.subject_name}},
                        {":empty_list", new AttributeValue{ IsLSet = true}}
                    },

                    //Set the id and name. Append to recommendatino list if exists, else set to empty list.
                    UpdateExpression = "SET #R = list_append(if_not_exists(#R, :empty_list), :recc), #I = :id, #N = :name"
                };

                var response = await client.UpdateItemAsync(request);

                return new APIGatewayProxyResponse
                {
                    StatusCode = 200
                };
            }
        }
    }
}
