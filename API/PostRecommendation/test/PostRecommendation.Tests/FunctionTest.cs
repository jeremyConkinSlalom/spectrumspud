using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using PostRecommendation;
using static PostRecommendation.Function;
using System.Net;
using Amazon.Lambda.APIGatewayEvents;
using Newtonsoft.Json;

namespace PostRecommendation.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestValidId()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            var recommendation = new NewRecommendation
            {
                subject_id = "12345",
                subject_name = "Aaron Knabb",
                date = "10/25/2018",
                recommendation = "test",
                recommender_id = "9999",
                recommender_name = "test name"
            };
            var request = new APIGatewayProxyRequest{Body = JsonConvert.SerializeObject(recommendation)};
            var value = function.FunctionHandler(request, context).Result;

            Assert.Equal(200, value.StatusCode);
        }
    }
}
